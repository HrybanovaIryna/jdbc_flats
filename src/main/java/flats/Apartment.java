package flats;

public class Apartment {
    private int id;
    private int number;
    private int square;
    private int price;

    Apartment(int id, int number, int square, int price) {
        this.id = id;
        this.number = number;
        this.square = square;
        this.price = price;
    }

    Apartment(int id, int price) {
        this.id = id;
        this.price = price;
    }


    @Override
    public String toString() {
        return "Apartment{" +
                "id=" + id +
                ", number=" + number +
                ", square=" + square +
                ", price=" + price +
                '}';
    }
}
