package flats;

import java.util.ArrayList;
import java.util.List;

public class District {
    private final int id;
    private final String name;
    private final List<Street> streets = new ArrayList<Street>();

    District(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addStreet(Street street) {
        streets.add(street);
    }

    @Override
    public String toString() {
        return "District{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", streets=" + streets +
                '}';
    }
}
