package flats;

import java.sql.*;

public class Main {
    public static void main(String[] arqs) {
        String url = "jdbc:mysql://localhost:3306/flats?serverTimezone=EET";
        String username = "root";
        String password = "root";

        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection connection = DriverManager.getConnection(url, username, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select district.id as district_id, district.name as district_name\n" +
                    "from district\n" +
                    "where id = 3;");

            while (resultSet.next()) {
                District district = new District(resultSet.getInt(1), resultSet.getString(2));
                System.out.println(district);
            }

            resultSet.close();
            statement.close();

            Statement statement1 = connection.createStatement();
            ResultSet resultSet1 = statement1.executeQuery("select apartment.id as apartment_id, apartment.price as apartment_price, " +
                    "building.id as building_id, building.number as building_number," +
                    " street.id as street_id, street.name as street_name, " +
                    "district.id as district_id, district.name as district_name " +
                    "from apartment " +
                    "join building on apartment.id_building = building.id " +
                    "join street on building.id_street = street.id " +
                    "join district on street.id_district = district.id " +
                    "where price > 100 and district.name = 'Holosiivskyi'");
            while (resultSet1.next()) {
                Apartment apartment = new Apartment(resultSet1.getInt("apartment_id"), resultSet1.getInt("apartment_price"));
                Building building = new Building(resultSet1.getInt("building_id"), resultSet1.getInt("building_number"));
                building.addApartment(apartment);
                Street street = new Street(resultSet1.getInt("street_id"), resultSet1.getString("street_name"));
                street.addBuilding(building);
                District district = new District(resultSet1.getInt("district_id"), resultSet1.getString("district_name"));
                district.addStreet(street);
                System.out.println(district);
            }

            resultSet1.close();
            statement1.close();

            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}


