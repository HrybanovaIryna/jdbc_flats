package flats;

import java.util.ArrayList;
import java.util.List;

public class Street {
    private final int id;
    private final String name;
    private final List<Building> buildings = new ArrayList<Building>();

    Street(int id, String name) {
        this.id = id;
        this.name = name;

    }

    public void addBuilding(Building building) {
        buildings.add(building);
    }

    @Override
    public String toString() {
        return "Street{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", buildings=" + buildings +
                '}';
    }
}
