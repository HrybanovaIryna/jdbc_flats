package flats;

import java.util.ArrayList;
import java.util.List;

public class Building {
    private final int id;
    private final int number;
    private final List<Apartment> apartments = new ArrayList<Apartment>();

    Building(int id, int number) {
        this.id = id;
        this.number = number;

    }

    @Override
    public String toString() {
        return "Building{" +
                "id=" + id +
                ", number=" + number +
                ", apartments=" + apartments +
                '}';
    }

    public void addApartment(Apartment apartment) {
        apartments.add(apartment);
    }

}
